import {
  ADD_CATEGORY,
  REMOVE_CATEGORY,
  CHANGE_CATEGORY_TITLE,
  CHANGE_CATEGORY_DESCRIPTION
} from './types';
import getRandomId from './getRandomId.util';

export const addCategory = parent => ({
  type: ADD_CATEGORY,
  payload: { id: getRandomId(), parent }
});

export const removeCategory = id => ({
  type: REMOVE_CATEGORY,
  payload: { id }
});

export const changeCategoryTitle = (id, value) => ({
  type: CHANGE_CATEGORY_TITLE,
  payload: { id, value }
});

export const changeCategoryDescription = (id, value) => ({
  type: CHANGE_CATEGORY_DESCRIPTION,
  payload: { id, value }
});
