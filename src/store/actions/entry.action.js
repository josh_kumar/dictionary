import {
  ADD_ENTRY,
  REMOVE_ENTRY,
  CHANGE_ENTRY_KEY,
  CHANGE_ENTRY_VALUE,
  UPDATE_ENTRY_WARNING
} from './types';
import getRandomId from './getRandomId.util';

export const addEntry = parent => ({
  type: ADD_ENTRY,
  payload: { id: getRandomId(), parent }
});

export const removeEntry = id => ({
  type: REMOVE_ENTRY,
  payload: { id }
});

export const changeEntryKey = (id, value) => ({
  type: CHANGE_ENTRY_KEY,
  payload: { id, value }
});

export const changeEntryValue = (id, value) => ({
  type: CHANGE_ENTRY_VALUE,
  payload: { id, value }
});

export const updateEntryWarning = (id, value) => ({
  type: UPDATE_ENTRY_WARNING,
  payload: { id, value }
});
