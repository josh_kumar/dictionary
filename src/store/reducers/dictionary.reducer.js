import produce from 'immer';
import {
  DICTIONARY_ID,
  CATEGORY,
  ENTRY,
  ADD_CATEGORY,
  REMOVE_CATEGORY,
  CHANGE_CATEGORY_TITLE,
  CHANGE_CATEGORY_DESCRIPTION,
  ADD_ENTRY,
  REMOVE_ENTRY,
  CHANGE_ENTRY_KEY,
  CHANGE_ENTRY_VALUE,
  UPDATE_ENTRY_WARNING
} from '../actions/types';

const initialState = {
  [DICTIONARY_ID]: {
    title: 'Custom Dictionary',
    description: 'this is a custom dictionary',
    data: [],
    type: CATEGORY
  }
};

const removeEl = (state, id) => {
  if (state[id].data) {
    state[id].data.forEach(uid => removeEl(state, uid));
  }

  delete state[id];
};

export default (state = initialState, action) => {
  let newState = state;

  switch (action.type) {
    case ADD_CATEGORY: {
      const { id, parent } = action.payload;

      newState = produce(state, draft => {
        draft[id] = {
          title: '',
          description: '',
          data: [],
          type: CATEGORY,
          parent
        };

        draft[parent].data.push(id);
      });

      break;
    }

    case REMOVE_CATEGORY: {
      const { id } = action.payload;
      const { parent } = state[id];

      newState = produce(state, draft => {
        draft[id].data.forEach(uid => removeEl(draft, uid));
        draft[parent].data = draft[parent].data.filter(uid => uid !== id);
        delete draft[id];
      });

      break;
    }

    case CHANGE_CATEGORY_TITLE: {
      const { id, value } = action.payload;

      newState = produce(state, draft => {
        draft[id].title = value;
      });

      break;
    }

    case CHANGE_CATEGORY_DESCRIPTION: {
      const { id, value } = action.payload;

      newState = produce(state, draft => {
        draft[id].description = value;
      });

      break;
    }

    case ADD_ENTRY: {
      const { id, parent } = action.payload;

      newState = produce(state, draft => {
        draft[id] = {
          key: '',
          value: '',
          type: ENTRY,
          warning: false,
          parent
        };

        draft[parent].data.push(id);
      });

      break;
    }

    case REMOVE_ENTRY: {
      const { id } = action.payload;

      newState = produce(state, draft => {
        const { parent } = draft[id];
        draft[parent].data = draft[parent].data.filter(uid => uid !== id);
        delete draft[id];
      });

      break;
    }

    case CHANGE_ENTRY_KEY: {
      const { id, value } = action.payload;

      newState = produce(state, draft => {
        draft[id].key = value;
      });

      break;
    }

    case CHANGE_ENTRY_VALUE: {
      const { id, value } = action.payload;

      newState = produce(state, draft => {
        draft[id].value = value;
      });

      break;
    }

    case UPDATE_ENTRY_WARNING: {
      const { id, value } = action.payload;

      newState = produce(state, draft => {
        draft[id].warning = value;
      });
    }

    default:
      break;
  }

  return newState;
};
