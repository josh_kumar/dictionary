import { combineReducers } from 'redux';

import dictReducer from './dictionary.reducer';

export default combineReducers({
  dictionary: dictReducer
});
