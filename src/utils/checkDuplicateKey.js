import { ENTRY } from '../store/actions/types';

export default (dict, id, value) => {
  return !!Object.entries(dict).find(
    entry => entry[1].key === value && entry[1].type === ENTRY && entry[0] !== id
  );
};
