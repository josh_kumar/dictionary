import React from 'react';

import Category from './components/category/category';
import Entry from './components/entry/entry';
import Dictionary from './components/dictionary/dictionary';

const App = () => (
  <Dictionary>
    <Category>
      <Entry />
    </Category>
  </Dictionary>
);

export default App;
