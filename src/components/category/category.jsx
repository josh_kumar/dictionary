import React, { useState, createRef } from 'react';

import { connect } from 'react-redux';

import deleteSVG from './delete.svg';
import editSVG from './edit.svg';
import addFolderSVG from './add-folder.svg';
// import arrowSVG from './sort-down.svg';

import {
  CategoryBlockStyled,
  CategoryStyled,
  ExpandStyled,
  TitleStyled,
  SummaryStyled,
  DrawerStyled,
  TitleContainerStyled
} from './category.style';
import { ButtonStyled, TaskBarStyled } from '../icons/icons.style';
import { CATEGORY } from '../../store/actions/types';
import {
  changeCategoryTitle,
  changeCategoryDescription,
  addCategory,
  removeCategory
} from '../../store/actions/category.action';

import Entry from '../entry/entry';
import { addEntry } from '../../store/actions/entry.action';

const Category = ({
  dictionary,
  uid,
  changeTitle,
  changeDescription,
  newCategory,
  newEntry,
  deleteCategory
}) => {
  const descriptionRef = createRef();
  const [open, setOpen] = useState(false);

  const openHandler = () => {
    setOpen(!open);
  };

  const alwaysOpenHandler = () => {
    setOpen(true);
  };

  const titlePressHandler = e => {
    if (e.key === 'Enter') {
      descriptionRef.current.focus();
    }
  };

  const descriptionPressHandler = e => {
    if (e.key === 'Enter') {
      newEntry(uid);
    }
  };

  const titleChangeHandler = e => changeTitle(uid, e.target.value);
  const descriptionChangeHandler = e => changeDescription(uid, e.target.value);
  const newCategoryHandler = () => newCategory(uid);
  const newEntryHandler = () => newEntry(uid);
  const deleteCategoryHandler = () => deleteCategory(uid);

  const { title, description, data } = dictionary[uid];

  return (
    <CategoryBlockStyled>
      <ExpandStyled onClick={openHandler}>{open ? '-' : '+'}</ExpandStyled>
      <CategoryStyled onClick={alwaysOpenHandler}>
        <TitleContainerStyled>
          <TitleStyled
            placeholder="Title"
            value={title}
            onChange={titleChangeHandler}
            onKeyPress={titlePressHandler}
          />
          <TaskBarStyled>
            <ButtonStyled onClick={newCategoryHandler}>
              <img src={addFolderSVG} alt="" />
            </ButtonStyled>
            <ButtonStyled onClick={newEntryHandler}>
              <img src={editSVG} alt="" />
            </ButtonStyled>
            <ButtonStyled onClick={deleteCategoryHandler}>
              <img src={deleteSVG} alt="" />
            </ButtonStyled>
          </TaskBarStyled>
        </TitleContainerStyled>
        <DrawerStyled open={open}>
          <SummaryStyled
            placeholder="description"
            value={description}
            onChange={descriptionChangeHandler}
            onKeyPress={descriptionPressHandler}
            ref={descriptionRef}
          />
          {data.map(id =>
            dictionary[id].type === CATEGORY ? (
              <CategoryHOC key={id} uid={id} />
            ) : (
              <Entry key={id} uid={id} />
            )
          )}
        </DrawerStyled>
      </CategoryStyled>
    </CategoryBlockStyled>
  );
};

const mapStateToProps = ({ dictionary }) => ({ dictionary });

const mapDispatchToProps = dispatch => ({
  changeTitle: (id, value) => dispatch(changeCategoryTitle(id, value)),
  changeDescription: (id, value) => dispatch(changeCategoryDescription(id, value)),
  newCategory: parent => dispatch(addCategory(parent)),
  newEntry: parent => dispatch(addEntry(parent)),
  deleteCategory: id => dispatch(removeCategory(id))
});

const CategoryHOC = connect(mapStateToProps, mapDispatchToProps)(Category);

export default CategoryHOC;
