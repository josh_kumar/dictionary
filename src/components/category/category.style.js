import styled from 'styled-components';

export const CategoryBlockStyled = styled.div`
  display: flex;
  margin-bottom: 5px;

  & input {
    outline: none;
    padding: 0px 5px;
    background: rgba(255, 255, 255, 0.5);

    &:focus {
      background: #eee;
    }
  }
`;

export const CategoryStyled = styled.div`
  flex-grow: 1;
`;

export const ExpandStyled = styled.button`
  width: 16px;
  height: 20px;
  margin: 4px;
  padding: 0px;
`;

export const TitleStyled = styled.input`
  display: block;
  flex: 1;
  width: fill-available;
  text-align: left;
  border: none;
  font-size: 16px;
`;

export const TitleContainerStyled = styled.div`
  display: flex;
`;

export const SummaryStyled = styled.input`
  flex: 1;
  width: fill-available;
  padding: 5px !important;
  text-align: left;
  border: none;
`;

export const DrawerStyled = styled.div`
  transition: 0.2s;
  display: ${({ open }) => (open ? 'flex' : 'none')};
  flex-direction: column;
  width: inherit;
`;
