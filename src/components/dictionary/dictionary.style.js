import styled from 'styled-components';
import backgroundImg from './background2.jpg';

const Dictionary = styled.div`
  position: absolute;
  padding: 10px;
  background: url(${backgroundImg});
  top: 0px;
  bottom: 0px;
  left: 0px;
  right: 0px;
  font-family: 'Open Sans', sans-serif;
  // display: ${({ warning }) => (warning ? 'block' : 'none')};
`;

export default Dictionary;
