import React from 'react';

import Cateogory from '../category/category';
import Dictionary from './dictionary.style';
import { DICTIONARY_ID } from '../../store/actions/types';
import Search from '../search/search';

const DictionaryBlock = () => (
  <Dictionary>
    <Search />
    <Cateogory uid={DICTIONARY_ID} />
  </Dictionary>
);

export default DictionaryBlock;
