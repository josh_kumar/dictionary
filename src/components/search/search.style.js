import styled from 'styled-components';

export const SearchContainerStyled = styled.div`
  display: flex;
  padding: 10px;
  flex-grow: 1;
`;

export const SearchInputStyled = styled.input`
  width: fill-available;
  margin-left: 10px;
  border-radius: 15px;
  height: 20px;
  border: 1px solid lightgrey;
  font-size: 15px;
  padding: 5px 15px;
  outline: none;
  box-shadow: 20px 20px 60px #8687cf, -20px -20px 60px #b6b7ff;
`;

export const SearchBoxStyled = styled.div`
  flex-grow: 1;
  position: relative;
`;

export const SearchTextStyled = styled.div`
  display: flex;
  align-items: center;
  height: 32px;
`;

export const SearchResultList = styled.div`
  background: white;
  margin-left: 10px;
  border: 1px solid lightgrey;
  border-top: none;
  position: absolute;
  top: 32px;
  width: fill-available;
  border-radius: 15px;
`;

export const SearchResultItem = styled.div`
  padding: 5px 15px;
`;

export const SearchItemKey = styled.div`
  font-size: 14px;
`;

export const SearchItemValue = styled.div`
  font-size: 12px;
`;

export const SearchItemType = styled.span`
  font-size: 10px;
  font-style: italic;
`;

export const SearchNoResult = styled.div`
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
`;
