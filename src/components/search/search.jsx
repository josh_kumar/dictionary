import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import {
  SearchContainerStyled,
  SearchInputStyled,
  SearchResultList,
  SearchBoxStyled,
  SearchTextStyled,
  SearchResultItem,
  SearchItemKey,
  SearchItemType,
  SearchItemValue,
  SearchNoResult
} from './search.style';

export default () => {
  const [searchText, setSearchText] = useState('');
  const searchResult = useSelector(state => {
    if (searchText === '') return [];
    return Object.entries(state.dictionary).filter(entry => {
      if (entry[1].key && entry[1].key.includes(searchText)) {
        return true;
      }

      if (entry[1].value && entry[1].value.includes(searchText)) {
        return true;
      }

      if (entry[1].title && entry[1].title.includes(searchText)) {
        return true;
      }

      if (entry[1].description && entry[1].description.includes(searchText)) {
        return true;
      }

      return false;
    });
  }, []);

  const searchChangeHandler = e => {
    setSearchText(e.target.value);
  };

  return (
    <SearchContainerStyled>
      <SearchTextStyled>Search</SearchTextStyled>
      <SearchBoxStyled>
        <SearchInputStyled value={searchText} onChange={searchChangeHandler} />
        {searchText === '' ? null : (
          <SearchResultList>
            {searchResult.length > 0 ? (
              searchResult.map(item => (
                <SearchResultItem key={item[0]}>
                  <SearchItemKey>
                    {item[1].key || item[1].title}
                    <SearchItemType>{` ${item[1].type}`}</SearchItemType>
                  </SearchItemKey>
                  <SearchItemValue>{item[1].value || item[1].description}</SearchItemValue>
                </SearchResultItem>
              ))
            ) : (
              <SearchNoResult>No Result Found</SearchNoResult>
            )}
          </SearchResultList>
        )}
      </SearchBoxStyled>
    </SearchContainerStyled>
  );
};
