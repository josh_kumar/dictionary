import styled from 'styled-components';

export const EntryStyled = styled.div`
  margin: 0px;
  display: flex;
  margin-left: 30px;
`;

export const ValueStyled = styled.input`
  flex: 1;
  text-align: left;
  border: 1px solid lightgrey;
  border-left: none;
  font-size: 13px;
`;

export const KeyStyled = styled.input`
  width: 200px;
  text-align: left;
  border: 1px solid lightgrey;
  font-size: 13px;
`;

export const EntryWarnStyled = styled.div`
  margin-left: 30px;
  font-size: 10px;
  color: orange;
  display: ${({ warning }) => (warning ? 'block' : 'none')};
`;
