import React, { createRef } from 'react';

import { connect } from 'react-redux';

import { EntryStyled, KeyStyled, ValueStyled, EntryWarnStyled } from './entry.style';
import { ButtonStyled } from '../icons/icons.style';
import cancelSVG from './cancel.svg';
import {
  changeEntryKey,
  changeEntryValue,
  removeEntry,
  addEntry,
  updateEntryWarning
} from '../../store/actions/entry.action';
import checkDuplicateKey from '../../utils/checkDuplicateKey';

const Entry = ({
  uid,
  dictionary,
  changeKey,
  changeValue,
  deleteEntry,
  newEntry,
  updateWarning
}) => {
  const valueRef = createRef();

  const keyChangeHandler = e => {
    const {
      target: { value }
    } = e;

    changeKey(uid, value);
    setTimeout(() => {
      const showWarning = checkDuplicateKey(dictionary, uid, value);
      updateWarning(uid, showWarning);
    }, 0);
  };

  const valueChangeHandler = e => changeValue(uid, e.target.value);

  const deleteHandler = () => deleteEntry(uid);

  const keyPressHandler = e => {
    if (e.key === 'Enter') {
      valueRef.current.focus();
    }
  };

  const valuePressHandler = e => {
    if (e.key === 'Enter') {
      newEntry(dictionary[uid].parent);
    }
  };

  const { key, value, warning } = dictionary[uid];

  return (
    <>
      <EntryStyled>
        <KeyStyled value={key} onChange={keyChangeHandler} onKeyPress={keyPressHandler} autoFocus />
        <ValueStyled
          value={value}
          onChange={valueChangeHandler}
          ref={valueRef}
          onKeyPress={valuePressHandler}
        />
        <ButtonStyled onClick={deleteHandler}>
          <img src={cancelSVG} alt="" />
        </ButtonStyled>
      </EntryStyled>
      <EntryWarnStyled warning={warning}>
        <span role="img" aria-label="warning">
          ⚠️
        </span>
        Entry Already Exists
      </EntryWarnStyled>
    </>
  );
};

const mapStateToProps = ({ dictionary }) => ({ dictionary });

const mapDispatchToProps = dispatch => ({
  changeKey: (id, value) => dispatch(changeEntryKey(id, value)),
  changeValue: (id, value) => dispatch(changeEntryValue(id, value)),
  deleteEntry: id => dispatch(removeEntry(id)),
  newEntry: id => dispatch(addEntry(id)),
  updateWarning: (id, value) => dispatch(updateEntryWarning(id, value))
});

export default connect(mapStateToProps, mapDispatchToProps)(Entry);
