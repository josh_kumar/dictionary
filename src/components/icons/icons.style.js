import styled from 'styled-components';

export const TaskBarStyled = styled.div`
  display: flex;
`;

export const ButtonStyled = styled.div`
  width: 20px;
  height: 20px;
  margin: 5px 5px;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;

  &:hover {
    background: lightgrey;
  }

  & > img {
    width: 15px;
    height: 15px;
  }
`;
